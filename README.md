# react-preset

Basically an experimental project with the latest web tech I find interesting and could see myself using.

Inlcuding:

- Complex State Management
- SSR
- Styled Components
- Code Splitting
- Routing

TODO:

- Server Side Data loading (exec actions on server)
- SWR/React Query like cache layer for requests integrated with global state

