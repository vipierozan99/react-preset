const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin").default;
const nodeExternals = require("webpack-node-externals");
const LoadablePlugin = require("@loadable/webpack-plugin");

const BaseConfig = (env, argv) => ({
  mode: env.prod || "development",
  resolve: {
    extensions: [".js", ".jsx"],
    plugins: [
      new TsconfigPathsPlugin({
        extensions: [".js"],
        configFile: "./tsconfig.json",
      }),
    ],
  },
  optimization: {
    splitChunks: {
      chunks: "all",
    },
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: "babel-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.(png|jpe?g|gif)$/,
        type: "asset/resource",
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: __dirname + "/assets/index.html",
      filename: "index.html",
      inject: "body",
    }),
  ],
});

const ClientConfig = (env, argv) => ({
  entry: "./src/index.js",
  name: "client",
  devtool: env.prod ? false : "eval-source-map",
  target: "web",
  output: {
    path: path.resolve(__dirname, "build/client"),
    filename: "[name].js",
    chunkFilename: "[id].js",
    publicPath: "/",
  },
  devServer: {
    hot: true,
    historyApiFallback: true,
  },
  ...BaseConfig(env, argv),
});

const ServerConfig = (env, argv) => {
  const config = {
    entry: "./scripts/ssr_server.js",
    name: "server",
    devtool: env.prod ? false : "eval-source-map",
    target: "node",
    externalsPresets: { node: true }, // in order to ignore built-in modules like path, fs, etc.
    externals: [nodeExternals()], // in order to ignore all modules in node_modules folder
    output: {
      path: path.resolve(__dirname, "build/server"),
      filename: "ssr_server.js",
      chunkFilename: "[id].js",
    },
    ...BaseConfig(env, argv),
  };

  config.plugins.push(new LoadablePlugin());
  return config;
};
module.exports = [ClientConfig, ServerConfig];
module.exports.parallelism = 4;
