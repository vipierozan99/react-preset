import create from "zustand";

/**
 * @typedef {Object} Pokemon
 * @property {number} id
 * @property {string} name
 * @property {string} pic
 * @property {string[]} types
 */

export const createPokemonStore = set => {
  const state = {
    /** @type {Pokemon | null} */
    currentPokemon: null,
    /** @param {number} id */
    fetchPokemon: async id => {
      const response = await (
        await fetch(`https://pokeapi.co/api/v2/pokemon/${id}`)
      ).json();

      set({
        currentPokemon: {
          id,
          name: response.species.name,
          pic: response.sprites.front_default,
          types: response.types.map(t => t.type.name),
        },
      });
    },
  };

  return state;
};

export default create(createPokemonStore);
