import React, { useEffect } from "react";
import shallow from "zustand/shallow";
import { Link, useParams } from "react-router-dom";
import usePokemon from "@Stores/pokemon";
import PokemonCard from "@Components/PokemonCard";

/** @param {ReturnType<import("@Stores/pokemon").createPokemonStore>} state */
const selector = state => ({
  currentPokemon: state.currentPokemon,
  fetchPokemon: state.fetchPokemon,
});

const ViewPokemon = () => {
  const { id } = useParams();
  const { currentPokemon, fetchPokemon } = usePokemon(selector, shallow);

  useEffect(() => {
    fetchPokemon(id);
  }, [id]);

  return (
    <div>
      <div>
        <Link to="/">Home</Link>
      </div>
      {currentPokemon ? (
        <PokemonCard pokemon={currentPokemon} />
      ) : (
        <h1>No Pokemon</h1>
      )}
    </div>
  );
};

export default ViewPokemon;
