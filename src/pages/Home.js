import React, { useState } from "react";
import { Link } from "react-router-dom";

const Home = () => {
  const [count, setCount] = useState(0);

  const btnClick = () => {
    console.log(count);
    setCount(count + 1);
  };

  return (
    <div>
      <Link to="/pokemon/1">View pokemon 1</Link>
      <h1>Home</h1>
      <button onClick={btnClick}>{count}</button>
    </div>
  );
};

export default Home;
