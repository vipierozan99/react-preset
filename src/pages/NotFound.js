import React from "react";
import { Link } from "react-router-dom";

const NotFound = () => {
  return (
    <div>
      <h1>404: Not FOund</h1>
      <div>
        <Link to="/">GoHome</Link>
      </div>
    </div>
  );
};

export default NotFound;
