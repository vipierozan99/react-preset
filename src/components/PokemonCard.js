import React from "react";
import BaseCard from "./BaseCard";

const PokemonCard = ({ pokemon }) => {
  return (
    <BaseCard>
      <h3>{pokemon.name}</h3>
      <img src={pokemon.pic} />
      <ul>
        {pokemon.types.map(t => (
          <li>{t}</li>
        ))}
      </ul>
    </BaseCard>
  );
};

export default PokemonCard;
