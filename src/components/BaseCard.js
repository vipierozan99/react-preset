import React from "react";

import styled from "styled-components";

const BaseCard = styled.button`
  text-align: center;
  margin: auto;
  padding: 0.25em;
  padding-top: 0.5em;
  padding-bottom: 0.5em;
  border-style: solid;
  border-radius: 0.25em;
  border-color: black;
  border-width: 1px;
  cursor: pointer;
  font-size: 15px;
  font-family: "Tahoma";
  min-width: 6em;
`;

export default BaseCard;
