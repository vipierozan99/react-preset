import React from "react";
import { Switch, Route } from "react-router-dom";
import loadable from "@loadable/component";

const Home = loadable(() => import(/* webpackPrefetch: true */ "./pages/Home"));
const NotFound = loadable(() =>
  import(/* webpackPrefetch: true */ "./pages/NotFound"),
);
const ViewPokemon = loadable(() =>
  import(/* webpackPrefetch: true */ "./pages/ViewPokemon"),
);

const App = () => {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/pokemon/:id" component={ViewPokemon} />
      <Route path="*" component={NotFound} />
    </Switch>
  );
};

export default App;
