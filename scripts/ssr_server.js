import express from "express";
import React from "react";
import path from "path";
import App from "../src/App";
import { renderToString } from "react-dom/server";
import { ServerStyleSheet } from "styled-components";
import { StaticRouter, redirect } from "react-router-dom";
import { ChunkExtractor } from "@loadable/server";

const app = express();
const port = 8080;

const statsFile = path.resolve("./build/server/loadable-stats.json");
const extractor = new ChunkExtractor({ statsFile });

app.use(express.static("./build/client"));

app.get("*", (req, res) => {
  let URL = req.url;
  const sheet = new ServerStyleSheet();
  const context = {};
  const reactApp = renderToString(
    extractor.collectChunks(
      sheet.collectStyles(
        <StaticRouter location={URL} context={context}>
          <App />
        </StaticRouter>,
      ),
    ),
  );

  if (context.url) {
    redirect(301, context.url);
    URL = context.url;
  }

  return res.send(`
    <!DOCTYPE html>
    <html>
      <head>
      ${extractor.getLinkTags()}
      ${extractor.getStyleTags()}
      </head>
      <body>
        <div id="root">${reactApp}</div>
        ${extractor.getScriptTags()}
      </body>
    </html>
  `);
});

app.listen(port, () => {
  console.log(`Server is listening on http://localhost:${8080}`);
});
